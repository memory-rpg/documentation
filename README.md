# Memory RPG Documentation

Global documentation for all repositories of [memory-rpg project](https://framagit.org/memory-rpg)

>___
> 📝 **"_I'm exactly where I'm supposed to be._"** - 💎
> ___
> Repository-specific project will be found directly ... in the repository ;)
> ___

Project repositories are :
* [backend](https://framagit.org/memory-rpg/backend) : some FastAPI, some 🦄 and some fun.
* [frontend](https://framagit.org/memory-rpg/frontend) : Svelt & Routify.
* [documentation](https://framagit.org/memory-rpg/documentation) : You are here 👍.

## Table of contents
- [Memory RPG Documentation](#memory-rpg-documentation)
  - [Table of contents](#table-of-contents)
  - [Contributing](#contributing)
  - [Application overview](#application-overview)
  - [Development philosophy](#development-philosophy)
  - [Developer setup - Tips & Tricks](#developer-setup---tips--tricks)
    - [Git hooks](#git-hooks)
    - [VSCode](#vscode)

## Contributing

If you are willing to contribute to the project, there are several ways :

1. 🚀 **Development** : You can start by installing your developer environment (there is some step-by-step documentation in each repo 💖) in few commands and [contact us](mailto:developermemoryrpg@gmail.com) so we can share the JIRA with you !
2. 📝**Documentation** : Improve the other contributors landing 🛬 and knowledge sharing by proof-reading, updating or creating new parts. You can start from [here](CONTRIBUTE.md).
3. 👩🏻‍🔬 **Testing** : For now, there is no _stable_ application deployed but if you want to give it a try a provide us feedback, feel free to [contact us](mailto:developermemoryrpg@gmail.com).

## Application overview

>___
>❌🗋 **That section needs to be completed.**
>___

## Development philosophy

Pull Requests, tests and experiments.

>___
>❌🗋 **That section needs to be completed.**
>___

## Developer setup - Tips & Tricks

A bit of everything which sounds useful people on the project.

### Git hooks

On your local environment, you can use [git hooks](https://git-scm.com/docs/githooks) which some automatically run commands when executing some `git` commands. It can be helpful to your environment more pleasant by avoiding to run (_again_!) the (_exact_!) same set of commands after doing a (_frequent_!) git command.

You can also use it to ensure all tests are run each time you commit|pull|push (that's up to you) to avoid the well-known situation "*Ah yes, I forgot to rename that variable in that file*".

>___
> ✔️ **Please, share your hooks with use ! :)**
> ___

Hooks being often repository specific, you will find more information direclty in there.

### VSCode 

We are using VSCode (because we like it) and so we like to download some plugins to improve it and/or to configure it. You will find some "key" plugins in each repository. 

But there are also [some setup tips being used on the project overall](developer/vscode_setup.md).
