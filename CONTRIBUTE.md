# Contribute to documentation

Documentation, even in fast-paced-agile-poc-mvp world, is key for product/application/code maintenance, evolution, re-use,... It is always welcome if you contribute to that documentation !

**Table of contents**
- [Contribute to documentation](#contribute-to-documentation)
  - [When and why should I update the doc ?](#when-and-why-should-i-update-the-doc-)
  - [Documentation file structure](#documentation-file-structure)
    - [Useful markdown formatting](#useful-markdown-formatting)
  - [Documentation folder structure](#documentation-folder-structure)

## When and why should I update the doc ?

- **Fixing various mistakes, typos, ...** _(Writer was just checking if you were reading anyway_ 🤓 _)._
- **Adding new/Updating existing section** : 
  - Pro-tips for developer setup or a known-issue to share ?
  - You updated a database model section and want to update informations about it ?
  - That famous critical configuration has been updated : better update the documentation about why and what.
- **Adding new page !**
  - Well new (big) feature, new doc ?
  - Could also be a existing documentation splitting (no-one likes loooong documentation pages
  - Check [Documentation file structure](#documentation-file-structure)
- **Adding new section !!**
  - Want to talk something brand new ? Like some code architecture or making some real deep-dive on how to write some app specific security checks ?
  - Check [Documentation folder structure](#documentation-folder-structure)

>___
> ✔️ **Doc = Code = Reviewed**
> ___
> Documentation being "as-code", it will be reviewed through a Merge/Pull Request anyway ! So do not be afraid about updating existing. 
> ___

In conclusion, you should update the documentation whenever you feel the need (or the reviewer ask you to 😎).

## Documentation file structure

The documentation is write in [Markdown](https://www.markdownguide.org/getting-started/) which is kind of very simple tag-oriented langage.  
Most of code editor does support that langage. Regarding VSCode, [Markdown All in One](https://marketplace.visualstudio.com/items?itemName=yzhang.markdown-all-in-one) is an useful plugin.

>___
> 💡 **Markdown Cheat Sheet**
> ___
> Wondering about how to ? Check [Markdown Cheat Sheet](https://www.markdownguide.org/cheat-sheet/).
> ___

In this project we usually expect a simple logic :

```md
# Title of documentation

Quick description / about.

[Table of contents]

## Section 1

<Your stuff here>

### Sub-section 1.a

<Your stuff here>
<Even more of your stuff below>

> ___
> ↩️ Browse back to [summary](./INDEX.md) page.

```

### Useful markdown formatting

Can allows your reader to quickly detect important informations !

```md
>___
> ⚠️ **Warning**
> ___
> Some important notice reader should be aware of.
> ___

>___
> 💡 **Tips**
> ___
> An helpful comment about whats going on.
> ___

>___
> 📝 **Info**
> ___
> An informative but important message.
> ___

>___
> ✔️ **Hurray !**
> ___
> An information about the fact everything should be smooth at that point.
> ___

>___
>❌🗋 **That section needs to be completed.**
>___
```


## Documentation folder structure

A folder contains :
- A `INDEX.md` file which list :
  - All documentation file in the current folder.
  - All sub-folders considered to be documentation sub-sections.
  >___
  > 💡 **All `INDEX.md` (structure) are the same !**
  > ___
  > You can copy any INDEX.md to have the general structure of it.
  > ___
- All documentations files related to the folder subject (which link the `INDEX.md` at the bottom, see [Documentation file structure](#documentation-file-structure)).

> ___
> ↩️ Browse back to [summary](./README.md) page.
