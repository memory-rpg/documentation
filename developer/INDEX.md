# Developer documentation

| File                              | Description                                         |
| :-------------------------------- | :-------------------------------------------------- |
| [../](../README.md)               | Back to previous documentation level.               |
| [Index](./INDEX.md)               | Current file.                                       |
| [VSCode setup](./vscode_setup.md) | Tips about VSCode setup for the project (and more). |

>___
> This is the end of the section : go back to [previous section](../README.md). 🔚
