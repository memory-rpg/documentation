# VSCode Setup

Few tips about VSCode setup being used in this project : you have one which is not listed here ? Please share it !

>___
> 💡 **All-in-one**
> ___
> We run VSCode at the root of the folder containing all our repositories, so it is easier to synchonize them and to switch from one to another. That said, do as you like !
> ___

- [VSCode Setup](#vscode-setup)
  - [Settings](#settings)
  - [Terminals auto-run](#terminals-auto-run)

## Settings

Code auto-formatting, plugins configuration, ... all that can be set in your settings. You can choose [User or Workspace settings](https://code.visualstudio.com/docs/getstarted/settings) (or a mix of both). If you're bored reading documentation, just copy paste that under `memory-rpg/.vscode/settings.json` :

```json
{
    "git.enableSmartCommit": true,
    "python.pythonPath": "${workspaceFolder}/.venv/Scripts/python.exe",
    "python.linting.pylintPath": "${workspaceFolder}/.venv/Scripts/pylint.exe",
    "python.linting.enabled": true,
    "python.linting.lintOnSave": true,
    "python.linting.pylintEnabled": true,
    "python.linting.pydocstyleEnabled": true,
    "python.linting.banditEnabled": false,
    "python.linting.pylintArgs": [
        "--max-line-length=120"
    ],
    "python.formatting.provider": "autopep8",
    "python.formatting.autopep8Args": [
        "--max-line-length",
        "120",
        "--experimental"
    ],
    "python.defaultInterpreterPath": "",
    "editor.codeActionsOnSave": {
        "source.organizeImports": true,
    },
    "editor.rulers": [
        120
    ],
    "editor.minimap.enabled": false,
    "html.format.wrapLineLength": 0,
    "editor.formatOnSave": true,
    "terminal.integrated.defaultProfile.windows": "Debian (WSL)",
    "terminal.integrated.profiles.windows": {
        "PowerShell": {
            "source": "PowerShell",
            "icon": "terminal-powershell"
        },
        "Command Prompt": {
            "path": [
                "${env:windir}\\Sysnative\\cmd.exe",
                "${env:windir}\\System32\\cmd.exe"
            ],
            "args": [],
            "icon": "terminal-cmd"
        },
        "Debian (WSL)": {
            "source": "Git Bash"
        }
    },
    "workbench.startupEditor": "none",
    "explorer.confirmDragAndDrop": false,
    "files.associations": {
        ".env": "ini"
    },
    "svelte.enable-ts-plugin": true,
    "javascript.updateImportsOnFileMove.enabled": "always",
    "terminal.integrated.persistentSessionReviveProcess": "never"
}
```

## Terminals auto-run

When you are all-in-one project you often want everything to be automatically launched to avoid re-typing same commands. Each. Time.

[Terminals Manager](https://marketplace.visualstudio.com/items?itemName=fabiospampinato.vscode-terminals) plugin is very good at that !

And here a configuration that can be put under `memory-rpg/.vscode/terminals.json` to automatically run backend and frontend (with auto-reload settings) and launching your bash directly in working folders :

```json
{
  "autorun": true,
  "autokill": true,
  "terminals": [
    {
      "name": "uvicorn",
      "description": "Open an uvicorn dedicated terminal",
      "icon": "server",
      "color": "green",
      "commands": [
        "source .venv/Scripts/activate",
        "cd backend",
        "python -m uvicorn app.main:app --host='0.0.0.0' --reload"
      ],
      "recycle": true
    },
    {
      "name": "backend",
      "description": "github - repo",
      "icon": "repo",
      "color": "cyan",
      "commands": [
        "source .venv/Scripts/activate",
        "cd backend",
        "git pull"
      ]
    },
    {
      "name": "svelte/routify",
      "description": "Open a svelte/routify dedicated terminal",
      "icon": "server",
      "color": "green",
      "commands": [
        "cd frontend/app",
        "npm run dev"
      ]
    },
    {
      "name": "frontend",
      "description": "github - repo",
      "icon": "repo",
      "color": "cyan",
      "focus": true,
      "commands": [
        "cd frontend/app",
        "npm install"
      ]
    },
    {
      "name": "documentation",
      "description": "github - repo",
      "icon": "repo",
      "color": "cyan",
      "focus": true,
      "command": "cd documentation"
    }
  ]
}
```

>___
> ✔️ **Hurray !**
> ___
> When opening VSCode project, you can start coding right away !
> ___

You can also combine it with some bash scripts for a wombo-combo.
